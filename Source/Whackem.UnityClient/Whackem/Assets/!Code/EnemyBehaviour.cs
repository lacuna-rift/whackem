﻿using Assets._Code;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public Transform Target;
    public float Speed = 0.1f;
    public float TimeFrequency = 0.5f;
    public Animator Animator;
    public short Health = 3;

    private bool _shouldMove = true;
    private float despawnTimer = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        Target = StateManager.Instance.Treasure[0].Transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Health <= 0)
        {
            despawnTimer -= Time.deltaTime;
            Animator.SetBool("Death", true);
        }

        if (despawnTimer <= 0)
        {
            Destroy(gameObject);
            return;
        }
        if (!_shouldMove)
        {
            Animator.SetFloat("Speed", -1);
            return;
        }

        Animator.SetFloat("Speed", 1);

        var step = AStar.Process(StateManager.Instance.Map, this);

        var now = transform.position;
        var target = new Vector3(step.Cell.Coordinates.x, transform.position.y, step.Cell.Coordinates.y);
        var next = new Ray(transform.position, target - transform.position).GetPoint(Speed);

        var distance1 = Vector3.Distance(now, target);
        var distance2 = Vector3.Distance(now, next);

        if (distance1 < distance2)
        {
            transform.position = target;
        }
        else
        {
            transform.position = next;
        }












        //var lerp = (transform.position - targetPos).normalized;
        //lerp.y = 0;


        //Debug.Log($"Transform position {transform.position.x}, {transform.position.y}, {transform.position.z}");
        //Debug.Log($"Speed {Speed}");
        //Debug.Log($"Lerp {lerp}");
        //Debug.Log($"Target {targetPos}");

        //var overshot = new Vector3((transform.position.x + (Speed * Mathf.Abs(lerp.x))), transform.position.y, (transform.position.z + (Speed * Mathf.Abs(lerp.z))));
        //_originalDirection = (transform.position - Target.position).normalized;

        //if ((transform.position.x + (Speed * Mathf.Abs(lerp.x))) > Mathf.Abs(targetPos.x) || (transform.position.z + (Speed * Mathf.Abs(lerp.z))) > Mathf.Abs(targetPos.z))
        //{
        //    Debug.Log($"End");
        //    transform.position = targetPos;
        //    _shouldMove = false;
        //}
        //else
        //{
        //    Debug.Log($"Move");
        //transform.position = new Vector3((transform.position.x + (Speed * Mathf.Abs(lerp.x))), transform.position.y, (transform.position.z + (Speed * Mathf.Abs(lerp.z))));
        //}
    }
}