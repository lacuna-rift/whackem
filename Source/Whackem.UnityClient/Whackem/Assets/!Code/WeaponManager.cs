﻿using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public void OnDrawGizmos()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(position.x, transform.position.y, position.z);
        if (Input.GetMouseButtonDown(0))
        {
            ReleaseIfClicked();
        }
    }

    private void ReleaseIfClicked()
    {
        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hitInfo, 100000, LayerMask.GetMask("Enemy")))
        {
            HitEnemy(hitInfo.collider.transform.parent.GetComponent<EnemyBehaviour>());
        }
    }

    private void HitEnemy(EnemyBehaviour enemy)
    {
        if (enemy?.Health == null) return;
        if (enemy.Health > 0)
        {
            enemy.Health--;
        }
    }
}