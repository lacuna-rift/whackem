﻿using Assets._Code.Enums;
using Assets._Code.Managers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayLevelBehaviour : MonoBehaviour
{
    public static PlayLevelBehaviour Instance { get; set; }

    public byte Level = 1;
    public Button PlayButton;
    public Image SelectionImage;

    public GameObject DisplayAvailable;
    public GameObject DisplayCompleted;
    public GameObject DisplayLocked;

    public LevelState LevelState;
    private LevelState LevelStateOld = LevelState.Locked;

    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnLevelSelection);
        PlayButton.onClick.AddListener(OnPlayLevel);
    }

    public void Update()
    {
        if (LevelStateOld == LevelState) return;

        SetLevelState(LevelState);
        Debug.Log($"Setting state to {LevelState}");
    }

    private void OnPlayLevel()
    {
        Debug.Log(LevelState);
        if (LevelState == LevelState.Locked) return;

        StateManager.Instance.CurrentLevel = Level;
        SceneManager.LoadScene("Game");
        Debug.Log($"Playing level {Level}");
    }

    private void OnLevelSelection()
    {
        transform.localScale = Vector3.one * 1.125f;
        SelectionImage.transform.gameObject.SetActive(true);

        transform.parent.GetComponent<LevelSelectionManager>().DeselectAllUnselectedChildren(Level);
    }

    public void SetLevelState(LevelState state)
    {
        // Available
        transform.GetChild(0).transform.gameObject.SetActive(state == LevelState.Current);
        
        // Completed
        if (state == LevelState.CompletedOneStar || 
            state == LevelState.CompletedTwoStar ||
            state == LevelState.CompletedThreeStar)
        {
            transform.GetChild(1).transform.gameObject.SetActive(true);
            transform.GetChild(1).GetChild(2).transform.gameObject.SetActive(state == LevelState.CompletedOneStar);
            transform.GetChild(1).GetChild(3).transform.gameObject.SetActive(state == LevelState.CompletedTwoStar);
            transform.GetChild(1).GetChild(4).transform.gameObject.SetActive(state == LevelState.CompletedThreeStar);
        }
        else
            transform.GetChild(1).transform.gameObject.SetActive(false);


        // Locked
        transform.GetChild(2).transform.gameObject.SetActive(state == LevelState.Locked);

        LevelStateOld = state;
    }

    public void Deselect()
    {
        transform.localScale = Vector3.one;
        SelectionImage.transform.gameObject.SetActive(false);
    }
}
