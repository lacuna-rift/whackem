﻿using UnityEngine;

public class StateManager : MonoBehaviour
{
    public static StateManager Instance { get; set; }

    public Map Map;
    public Treasure[] Treasure;
    
    public int CurrentLevel = 0;
    public int MenuFocus = 0;

    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public void OnDestroy()
    {
        Instance = null;
    }
}