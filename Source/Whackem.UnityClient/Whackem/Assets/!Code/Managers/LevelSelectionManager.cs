﻿using UnityEngine;

namespace Assets._Code.Managers
{
    public class LevelSelectionManager : MonoBehaviour
    {
        public void DeselectAllUnselectedChildren(short selectedLevel)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var playBehaviour = transform.GetChild(i).GetComponent<PlayLevelBehaviour>();
                if (playBehaviour.Level != selectedLevel)
                {
                    playBehaviour.Deselect();
                }
            }
        }
    }
}
