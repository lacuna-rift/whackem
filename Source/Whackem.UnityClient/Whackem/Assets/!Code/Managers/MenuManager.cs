﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets._Code.Managers
{
    public class MenuManager : MonoBehaviour
    {
        public Transform LevelPanel;
        public RectTransform MenuContainer;

        public Transform ItemPanel;

        public Text CurrencyText;

        private CanvasGroup fadeGroup;
        private float fadeInSpeed = 0.33f;

        private Vector3 _desiredMenuPosition;
        private int _previouslySelectedItem = -1;

        private void Start()
        {
            fadeGroup = FindObjectOfType<CanvasGroup>();
            fadeGroup.alpha = 1;

            SetCurrency(999);
            InitShop();
        }

        private void Update()
        {
            // Fade in
            fadeGroup.alpha = 1 - Time.timeSinceLevelLoad * fadeInSpeed;

            // Menu Navigation
            MenuContainer.anchoredPosition3D = Vector3.Lerp(MenuContainer.anchoredPosition3D, _desiredMenuPosition, 0.05f);
        }

        private void InitShop()
        {
            if (ItemPanel == null) return;

            for (int i = 0; i < ItemPanel.childCount; i++)
            {
                var currentIndex = i;
                var button = ItemPanel.GetChild(i).GetComponent<Button>();
                button.onClick.AddListener(() => OnItemSelection(currentIndex));
            }
        }

        public void SetCurrency(int value)
        {
            CurrencyText.text = value.ToString();
        }

        private void NavigateTo(int menuIndex)
        {
            switch (menuIndex)
            {
                // Main Menu
                default:
                case 0:
                    _desiredMenuPosition = Vector3.zero;
                    break;
                // Play Menu
                case 1:
                    _desiredMenuPosition = Vector3.right * 1920;
                    break;
                // Shop Menu
                case 2:
                    _desiredMenuPosition = Vector3.left * 1920;
                    break;
            }
        }

        private void OnItemSelection(int currentIndex)
        {
            if (_previouslySelectedItem == currentIndex) return;

            ItemPanel.GetChild(currentIndex).transform.localScale = Vector3.one * 1.125f;

            if (_previouslySelectedItem >= 0)
                ItemPanel.GetChild(_previouslySelectedItem).transform.localScale = Vector3.one;

            _previouslySelectedItem = currentIndex;
        }

        public void OnItemBuySet()
        {

        }

        public void OnPlayClick()
        {
            NavigateTo(1);
        }

        public void OnShopClick()
        {
            NavigateTo(2);
        }

        public void OnBackClick()
        {
            NavigateTo(0);
        }
    }
}
