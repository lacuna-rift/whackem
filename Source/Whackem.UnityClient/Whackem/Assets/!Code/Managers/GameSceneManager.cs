﻿using Assets._Code.Enums;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets._Code.Managers
{
    public class GameSceneManager : MonoBehaviour
    {
        public void CompleteLevel()
        {
            Debug.Log(PlayLevelBehaviour.Instance);
            PlayLevelBehaviour.Instance.SetLevelState(LevelState.CompletedTwoStar);
            //StateManager.Instance.CurrentLevel;
            StateManager.Instance.MenuFocus = 1;

            ExitScene();
        }

        public void ExitScene()
        {
            SceneManager.LoadScene("Mennu");
        }
    }
}
