﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets._Code
{
    class AStarStep
    {
        public MapCell Cell;
        public AStarStep Previous;
        public double CostSoFar;
    }

    class AStar
    {
        public static AStarStep Process(Map map, EnemyBehaviour unit)
        {
            if (unit?.transform == null) return null;

            var start = map.GetClosestCell(unit.transform.position);
            var destination = map.GetClosestCell(unit.Target.position);
            
            var frontier = new FastPriorityQueue<AStarStep>(map.Cells.Length);
            frontier.Enqueue(new AStarStep { CostSoFar = 0, Cell = start }, 0);

            var costSoFar = new Dictionary<Vector2Int, double>();

            while (frontier.Count > 0)
            {
                var currentStep = frontier.Dequeue();
                if (currentStep.Cell.Coordinates == destination.Coordinates) return currentStep;

                for (var x = -1; x <= 1; x++)
                {
                    for (var y = -1; y <= 1; y++)
                    {
                        if (x == 0 && y == 0) continue;

                        var nextCellCoordinate = currentStep.Cell.Coordinates + new Vector2Int(x, y);
                        var nextCell = map.GetAtPosition(nextCellCoordinate);

                        if (nextCell == null) continue;
                        if (!nextCell.CanMoveThrough) continue;


                        if (!costSoFar.TryGetValue(nextCell.Coordinates, out var nextCellCostSoFar) || currentStep.CostSoFar < nextCellCostSoFar)
                        {
                            costSoFar[nextCell.Coordinates] = currentStep.CostSoFar;
                            var priority = currentStep.CostSoFar + Heuristic( nextCell, destination.Coordinates);
                           // unit.transform.position = new Vector3(nextCell.Coordinates.x, unit.transform.position.y,  nextCell.Coordinates.y);
                            frontier.Enqueue(new AStarStep { CostSoFar = currentStep.CostSoFar, Cell = nextCell, Previous = currentStep }, priority);
                        }
                    }
                }
            }
            return null;
        }

        private static double Heuristic( MapCell from, Vector2Int to)
        {
            // Distance types 
            // https://iq.opengenus.org/content/images/2018/12/distance.jpg

            // We're using Euclidean Distance... AND SQRd, because im lazy <3
            return -Vector2Int.Distance(from.Coordinates, to);
        }

        private static bool UnitCanEnter(GameObject unit, MapCell from, MapCell to)
        {
            //if (to.Type == MapCellType.Rock) return false;
            //if (to.Type == MapCellType.Lake) return false;

            //if (Math.Abs(to.Height - from.Height) > 1) return false;
            return true;
        }

        private static double UnitTraverseCost(GameObject unit, MapCell from, MapCell to)
        {
            //switch (to.Type)
            //{
            //    case MapCellType.Desert: return 2;
            //    case MapCellType.Agriculture: return 1;

            //    case MapCellType.Road: return 0;
            //    case MapCellType.Flora: return 5;
            //    case MapCellType.Plains: return 1;
            //}
           // return 999;
           return 1;
        }
    }
}
