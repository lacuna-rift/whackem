﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject FloorPrefab;
    public GameObject Treasure1Prefab;
    public GameObject Treasure2Prefab;
    public GameObject Treasure3Prefab;
    public GameObject Treasure4Prefab;
    public bool Regen = false;
    public int Size = 10;

    void Awake()
    {
        StateManager.Instance.Treasure = GenerateTreasure();
        StateManager.Instance.Map = GenerateMap();

    }

    public void OnDrawGizmos()
    {
        if (!Regen)
        {
            return;
        }

        while (transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }

        GenerateTreasure();
        GenerateMap();

        Regen = false;
    }

    private Treasure[] GenerateTreasure()
    {
        var treasure = new List<Treasure>();

        var treasure1 = Instantiate(Treasure1Prefab, new Vector3(-1, 0.7f, 0), Quaternion.Euler(90, 0, 0), transform);
        treasure.Add(new Treasure() { Transform = treasure1.transform });

        var treasure2 = Instantiate(Treasure2Prefab, new Vector3(1, 0.7f, 0), Quaternion.Euler(90, 0, 0), transform);
        treasure.Add(new Treasure() { Transform = treasure2.transform });

        var treasure3 = Instantiate(Treasure3Prefab, new Vector3(0, 0.7f, -1), Quaternion.Euler(90, 0, 0), transform);
        treasure.Add(new Treasure() { Transform = treasure3.transform });

        var treasure4 = Instantiate(Treasure4Prefab, new Vector3(0, 0.7f, 1), Quaternion.Euler(90, 0, 0), transform);
        treasure.Add(new Treasure() { Transform = treasure4.transform });
        return treasure.ToArray();
    }

    private MapCell[] GenerateFloor()
    {
        var cells = new List<MapCell>();

        var positionX = (0 - (Size)) * 1;
        for (var x = 0; x < Size * 2; x++)
        {
            var positionZ = (0 - (Size / 2)) * 1;
            for (var z = 0; z < Size; z++)
            {
                var generated = Instantiate(FloorPrefab, new Vector3((float)positionX, 0, (float)positionZ), Quaternion.identity, transform);

                var cell = new MapCell()
                {
                    CanMoveThrough = true,
                    Coordinates = new Vector2Int(positionX, positionZ)
                };
                cells.Add(cell);

                positionZ += (int)FloorPrefab.transform.localScale.z;
            }

            positionX += (int)FloorPrefab.transform.localScale.x;
        }

        return cells.ToArray();
    }

    private Map GenerateMap()
    {
        var map = new Map();
        map.Cells = GenerateFloor()
            .OrderBy(o => o.Coordinates.x)
            .ThenBy(o => o.Coordinates.y)
            .ToArray();
        return map;
    }

    // Update is called once per frame
    void Update()
    {

    }
}

public class Map
{
    public MapCell[] Cells = { };

    public MapCell GetAtPosition(Vector2Int position)
    {
        foreach (var mapCell in Cells)
        {
            if (mapCell.Coordinates.x == position.x && mapCell.Coordinates.y == position.y)
            {
                return mapCell;
            }
        }

        return null;
    }

    public MapCell GetClosestCell(Vector3 position)
    {
        return Cells
            .OrderBy(o => Vector3.Distance(position, new Vector3Int(o.Coordinates.x, 0, o.Coordinates.y)))
            .FirstOrDefault();
    }
}
public class MapCell
{
    //public MapCellType Type;
    public Vector2Int Coordinates = new Vector2Int();
    public MapCell[] Neighbors = { };
    public bool CanMoveThrough = true;
}

public class Treasure
{
    public Transform Transform;
}