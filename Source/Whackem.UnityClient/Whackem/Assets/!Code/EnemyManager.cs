﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public int EnemyCount = 20;
    public float SpawnInterval = 5f;
    public GameObject EnemyPrefab;

    private float _timer;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _timer -= Time.deltaTime;
        if (_timer <= 0)
        {
            var random = new System.Random();
            _timer = SpawnInterval;
            var position = new Vector3(random.Next(5, EnemyCount), EnemyPrefab.transform.position.y, random.Next(5, EnemyCount));
            Debug.Log($"SPAWNING AT {position}");
            Instantiate(EnemyPrefab, position, Quaternion.identity);
        }
    }
}