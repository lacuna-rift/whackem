﻿namespace Assets._Code.Enums
{
    public enum LevelState : byte
    {
        Locked = 0,
        Current = 1,
        CompletedOneStar = 2,
        CompletedTwoStar = 3,
        CompletedThreeStar = 4,
    }
}
